package com.ricardorm13.hexagonal.dowStreamAdapter;

import com.ricardorm13.hexagonal.core.User;
import com.ricardorm13.hexagonal.outBoundPort.UserRepositoryInterface;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepository implements UserRepositoryInterface {
    private static final Map<Integer, User> userMap = new HashMap<>(0);

    UserSandbox sandbox = new UserSandbox();

    @Override
    public List<User> listaUsers(){
        return sandbox.listaUsuarios();
    }

    @Override
    public User obtemUserByID(Integer id) {
        List<User> lista = sandbox.listaUsuarios();
        for (Integer i=0; i < 3; i ++){
            User user = lista.get(i);
            if (user.getId() == id){
                return user;
            }
        }

        return null;
    }

    @Override
    public User obtemUserByNome(String nome) {
        List<User> lista = sandbox.listaUsuarios();
        for (Integer i=0; i < 3; i ++){
            User user = lista.get(i);
            if (user.getNome().equals(nome)){
                return user;
            }
        }
        return null;
    }
}
