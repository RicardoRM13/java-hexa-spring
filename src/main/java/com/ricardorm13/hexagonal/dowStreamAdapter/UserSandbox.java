package com.ricardorm13.hexagonal.dowStreamAdapter;

import com.ricardorm13.hexagonal.core.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserSandbox {
    static List<User> lista = new ArrayList<>();

    public UserSandbox(){
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();


        user1.setId(1);
        user1.setNome("user_1");
        user1.setEmail("user_1@email.com.br");

        user2.setId(2);
        user2.setNome("user_2");
        user2.setEmail("user_2@email.com.br");

        user3.setId(3);
        user3.setNome("user_3");
        user3.setEmail("user_3@email.com.br");

        lista.add(user1);
        lista.add(user2);
        lista.add(user3);

    }

    public List<User> listaUsuarios(){
        return lista;
    }
}
