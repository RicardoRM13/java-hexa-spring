package com.ricardorm13.hexagonal.core;

import com.ricardorm13.hexagonal.dowStreamAdapter.UserRepository;
import com.ricardorm13.hexagonal.inBoundPort.UserServiceInterface;
import com.ricardorm13.hexagonal.outBoundPort.UserRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplementation implements UserServiceInterface {
    @Autowired
    private UserRepositoryInterface userRepository;

    @Override
    public List<User> listaUsers() {
        return userRepository.listaUsers();
    }

    @Override
    public User obtemUserByID(Integer id) {
        return userRepository.obtemUserByID(id);
    }

    @Override
    public User obtemUserByNome(String nome) {
        return userRepository.obtemUserByNome(nome);
    }
}
