package com.ricardorm13.hexagonal.outBoundPort;

import com.ricardorm13.hexagonal.core.User;

import java.util.List;

public interface UserRepositoryInterface {
    List<User> listaUsers();

    User obtemUserByID(Integer id);

    User obtemUserByNome(String nome);
}
