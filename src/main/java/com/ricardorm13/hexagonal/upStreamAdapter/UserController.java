package com.ricardorm13.hexagonal.upStreamAdapter;

import com.ricardorm13.hexagonal.core.User;
import com.ricardorm13.hexagonal.inBoundPort.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UserController {
    private UserServiceInterface userServiceInterface;

    @Autowired
    public UserController(UserServiceInterface userServiceInterface){
        this.userServiceInterface = userServiceInterface;
    }

    @GetMapping
    public ResponseEntity<List<User>> listarUsuarios(){
        return  new ResponseEntity<List<User>>(userServiceInterface.listaUsers(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> obtemUserByID(@PathVariable Integer id){
        return new ResponseEntity<User>(userServiceInterface.obtemUserByID(id), HttpStatus.OK);
    }

    @GetMapping("/nomes/{nome}")
    public ResponseEntity<User> obtemUserByID(@PathVariable String nome){
        return new ResponseEntity<User>(userServiceInterface.obtemUserByNome(nome), HttpStatus.OK);
    }
}
