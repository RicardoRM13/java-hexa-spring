package com.ricardorm13.hexagonal.inBoundPort;

import com.ricardorm13.hexagonal.core.User;

import java.util.List;

public interface UserServiceInterface {
    List<User> listaUsers();

    User obtemUserByID(Integer id);

    User obtemUserByNome(String nome);
}
